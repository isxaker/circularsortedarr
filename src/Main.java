public class Main {

    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4, 5};
        int[] arr2 = {5, 6, 7, 8, 1, 2, 3, 4};
        int[] arr3 = {1};
        int[] arr4 = {1, 2};
        int[] arr5 = {2, 1};
        int[] arr6 = {5, 6, 7, 1, 2, 3, 4};
        int[] arr7 = {1, 2, 3, 4, 5, 6, 7};
        int[] arr8 = {2, 3, 4, 5, 6, 7, 8, 1};
        int[] arr9 = {3, 4, 5, 1, 2};
//test
//        int min1 = findMinIteratively(arr1);
//        int min2 = findMinIteratively(arr2);
//        int min3 = findMinIteratively(arr3);
//        int min4 = findMinIteratively(arr4);
//        int min5 = findMinIteratively(arr5);
//        int min6 = findMinIteratively(arr6);
//        int min7 = findMinIteratively(arr7);
//        int min8 = findMinIteratively(arr8);
//        int min9 = findMinIteratively(arr9);

//        int min1 = findMinRecursicely(arr1, 0, arr1.length - 1);
//        int min2 = findMinRecursicely(arr2, 0, arr2.length - 1);
//        int min3 = findMinRecursicely(arr3, 0, arr3.length - 1);
//        int min4 = findMinRecursicely(arr4, 0, arr4.length - 1);
//        int min5 = findMinRecursicely(arr5, 0, arr5.length - 1);
//        int min6 = findMinRecursicely(arr6, 0, arr6.length - 1);
//        int min7 = findMinRecursicely(arr7, 0, arr7.length - 1);
//        int min8 = findMinRecursicely(arr8, 0, arr8.length - 1);
//        int min9 = findMinRecursicely(arr9, 0, arr9.length - 1);

//        int max1 = findMaxRecursively(arr1, 0, arr1.length - 1);
//        int max2 = findMaxRecursively(arr2, 0, arr2.length - 1);
//        int max3 = findMaxRecursively(arr3, 0, arr3.length - 1);
//        int max4 = findMaxRecursively(arr4, 0, arr4.length - 1);
//        int max5 = findMaxRecursively(arr5, 0, arr5.length - 1);
//        int max6 = findMaxRecursively(arr6, 0, arr6.length - 1);
//        int max7 = findMaxRecursively(arr7, 0, arr7.length - 1);
//        int max8 = findMaxRecursively(arr8, 0, arr8.length - 1);
//        int max9 = findMaxRecursively(arr9, 0, arr9.length - 1);

        int x1 = binarySearch(arr1, 1);
        int x2 = binarySearch(arr2, 1);
        int x3 = binarySearch(arr3, 1);
        int x4 = binarySearch(arr4, 1);
        int x5 = binarySearch(arr5, 1);
        int x6 = binarySearch(arr6, 1);
        int x7 = binarySearch(arr7, 1);
        int x8 = binarySearch(arr8, 1);
        int x9 = binarySearch(arr9, 1);
    }

    public static int findMinIteratively(int[] arr) {
        int low = 0;
        int high = arr.length - 1;

        while (low < high) {

            int mid = low + (high - low) / 2;

            //check next after mid
            if (mid < high && arr[mid] > arr[mid + 1]) {
                return arr[mid + 1];
            }

            //check mid itself
            if (mid > low && arr[mid] < arr[mid - 1]) {
                return arr[mid];
            }

            //continue searching
            if (arr[mid] < arr[high]) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }

        //if there is only one element left
        //or array is not rotated
        return arr[0];
    }

    public static int findMaxRecursively(int[] arr, int low, int high) {

        //or array is not rotated
        if (low >= high) {
            return arr[arr.length - 1];
        }

        int mid = low + (high - low) / 2;

        if (mid < high && arr[mid] > arr[mid + 1]) {
            return arr[mid];
        }

        if (mid > low && arr[mid] < arr[mid - 1]) {
            return arr[mid - 1];
        }

        if (arr[mid] < arr[high]) {
            return findMaxRecursively(arr, low, mid - 1);
        } else {
            return findMaxRecursively(arr, mid + 1, high);
        }
    }

    public static int findMinRecursicely(int arr[], int low, int high) {
        // This condition is needed to handle the case when array
        // is not rotated at all
        if (high <= low) {
            return arr[0];
        }

//        // If there is only one element left
//        if (high == low) {
//            return arr[low];
//        }

        // Find mid
        int mid = low + (high - low) / 2; /*(low + high)/2;*/

        // Check if element (mid+1) is minimum element. Consider
        // the cases like {3, 4, 5, 1, 2}
        if (mid < high && arr[mid + 1] < arr[mid]) {
            return arr[mid + 1];
        }

        // Check if mid itself is minimum element
        if (mid > low && arr[mid] < arr[mid - 1]) {
            return arr[mid];
        }

        // Decide whether we need to go to left half or right half
        if (arr[high] > arr[mid]) {
            return findMinRecursicely(arr, low, mid - 1);
        } else {
            return findMinRecursicely(arr, mid + 1, high);
        }
    }

    public static int binarySearch(int arr[], int x) {
        int low = 0;
        int high = arr.length - 1;

        while (low <= high) {
            int mid = low + (high - low) / 2;

            if (x == arr[mid]) {
                return arr[mid];
            }

            if (arr[mid] < arr[high]) {
                if (x > arr[mid] && x <= arr[high]) {
                    low = mid + 1;
                } else {
                    high = mid - 1;
                }

            } else {
                if (x < arr[mid] && x >= arr[low]) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            }
        }

        return -1;
    }

}
